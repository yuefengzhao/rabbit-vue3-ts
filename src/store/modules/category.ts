import { defineStore } from 'pinia'
import request from '@/utils/request'
import { ApiRes, CategoryItem } from '@/types/data'
import { topCategory } from '../constants'

const defaultCategory = topCategory.map(item => ({ name: item }))

// interface CategoryItem {
//   id: string
//   name: string
//   picture: string
// }

// interface ApiRes<T> {
//   code: string
//   msg: string
//   result: T[]
// }

// defineStore 返回的是一个函数
export default defineStore('category', {
  state() {
    return {
      list: defaultCategory as CategoryItem[]
    }
  },
  actions: {
    async getAllCategory() {
      const res = await request.get<ApiRes<CategoryItem>>('/home/category/head')
      res.data.result.forEach(item => item.open = false)
      // console.log(res.data.result)
      this.list = res.data.result
    },
    show(id: string) {
      // 找到对应 id 的元素, 将其状态改为 true
      const item = this.list.find(item => item.id === id)
      // if (item) {
      //   item.open = true
      // }
      // 优雅的处理方法
      // item && (item.open = true)
      // 暴力处理法: 非空断言, 我断定你一定有 open
      item!.open = true
    },
    hide(id: string) {
      // 找到对应 id 的元素, 将其状态改为 true
      const item = this.list.find(item => item.id === id)
      item!.open = false
    }
  }
})