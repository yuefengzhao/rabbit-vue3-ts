// 所有的接口的通用类型
export interface ApiRes<T> {
    code: string
    msg: string
    result: T[]
  }
  
  // 单个分类的类型
  export interface CategoryItem {
    id: string
    name: string
    picture: string
    open: boolean
    // 既然子类一模一样，直接调用他自己即可，因为数据到第三层之后就为空了，所以不会死循环
    children: CategoryItem[]
  }
  