// 创建路由对象
import {createRouter,createWebHashHistory} from 'vue-router'
import Layout from '@/views/layout/index.vue'
import Home from '@/views/home/index.vue'
const router = createRouter({
routes:[
    {
        path:'/',
        component:Layout,
        children:[
            {
                // 默认路由
                path: '',
                component: Home,
            },
            // 下面都是二级路由
            {
                path:'category/:id',
                component:()=>import('@/views/category/index.vue')
            },
            {
                path:'category/sub/:id',
                component:()=>import('@/views/category/sub.vue')
            },
        ]
    },
    {
        path:'/login',
        component:()=>import('@/views/login/index.vue')
    }
],
history:createWebHashHistory()
})
// 导入路由对象
export default router